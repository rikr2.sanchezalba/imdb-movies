import Head from 'next/head';
import {ThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../styles/theme';
import '../styles/globals.css';
import {makeStyles} from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import {useState, useEffect, useContext} from 'react';
import Router from 'next/router';
import axios from 'axios';
import AxiosContext from "../components/Auth/AxiosContext";
import TopProgressBar from "../components/TopProgressBar/TopProgressBar";
import EGAMessageContext from "../components/EGAMessage/EGAMessageContext";
import useEGAMessage from "../components/EGAMessage/useEGAMessage";
import EGAMessage from "../components/EGAMessage/EGAMessage";
import dynamic from 'next/dynamic'





const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
    },
    footer: {
        padding: theme.spacing(3, 2),
        background: '#ababab',
        position: 'absolute',
        width: '100%',
        bottom: 0,
        sticky: 'true',
        zIndex: theme.zIndex.drawer + 1,
    },
}));

const App = ({Component, pageProps}) => {
    const {session} = pageProps;
    const classes = useStyles();
    const router = useRouter();
    const [user, setUser] = useState(undefined);
    const [loginMessage, setLoginMessage] = useState('');

    const {open, severity, message, showMessage, autoHideDuration, handleClose} = useEGAMessage()


    return (
        <div style={{height: '100%', width: '100%'}}>
            <Head>
                <title>{process.env.NEXT_PUBLIC_NAME}</title>
                <meta
                    name='viewport'
                    content='minimum-scale=1, initial-scale=1, width=device-width'
                />
            </Head>


                <ThemeProvider theme={theme}>

                    <CssBaseline/>
                    <AxiosContext.Provider value={{axios: axios}}>


                                <EGAMessageContext.Provider
                                    value={{open, severity, message, showMessage, autoHideDuration, handleClose}}>





                                                <>
                                                <div className={classes.root}>
                                                    <TopProgressBar/>
                                                </div>

                                                    <Component {...pageProps}/>

                                            </>







                                        <EGAMessage/>



                                </EGAMessageContext.Provider>


                    </AxiosContext.Provider>


                </ThemeProvider>


        </div>
    );
};

export default App;
