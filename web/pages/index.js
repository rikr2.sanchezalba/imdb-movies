import Head from 'next/head';
import {useState, useEffect, useContext, createRef, useMemo} from 'react';
import Box from '@material-ui/core/Box';
import {makeStyles} from '@material-ui/core/styles';
import {AutoSizer} from 'react-virtualized';
import Typography from '@material-ui/core/Typography';
import Egadatatable from "../components/EGADataTable/egadatatable";
import AxiosContext from "../components/Auth/AxiosContext";
import useLocalStorage from "../helpers/useLocalStorage";
import EGADataTableContext from "../components/EGADataTable/EGADataTableContext";
import Egapostercell from "../components/EGAPosterCell/egapostercell";
import NumFilter from "../components/NumFilter/numfilter";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        alignItems: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        justifyContent: 'center'
    }
}));

export default function Home() {
    const classes = useStyles();
    const {axios} = useContext(AxiosContext);
    const [filters, setFilters] = useLocalStorage('movieFilters', []);

    const renderPoster = (rowData) => {
        return <Egapostercell omdbUrl={rowData['poster']}/>
    }

    const renderLink = (rowData) => {
        return <a href={rowData['link']} color='blue'>IMDB link</a>
    }

    let [columns, setColumns] = useState()
    let [selectedColumns, setSelectedColumns] = useState()

    useEffect(()=>{
        let c = [

        {field: 'title', title: 'Title'},//
        {field: 'genres', title: 'Genres'},
        {field: 'year', title: 'Year',  filterComponent: (props) => <NumFilter {...props} fieldName={'year'} filters={filters} />},
        {field: 'rating', title: 'Rating', filterComponent: (props) => <NumFilter {...props} fieldName={'rating'}  filters={filters} />},
        {field: 'runtime', title: 'Runtime', filterComponent: (props) => <NumFilter {...props} fieldName={'runtime'}  filters={filters} />},
        {field: 'poster', title: 'Poster', render: renderPoster, filtering: false},
        {field: 'link', title: 'Link', render: renderLink, filtering: false},

    ]
        setColumns(c);
        setSelectedColumns(c);
    },[])



    const getData = query => {


        if (query.filters)
            setFilters(query.filters.map((item) => ({field: item.column.field, value: item.value})))



        const filters = query.filters
        // const orderBy = query.orderBy
        // const orderDirection =  query.orderDirection

        let data = {}
        if (filters.length > 0)
            data.filters = filters

        if (query.orderBy) {
            data.orderBy = query.orderBy.field
            data.orderDirection = query.orderDirection
        }


        return axios(
            {
                method: 'get',
                url: '/api/movies/',
                baseURL: process.env.API_HOST,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },

                params: {
                    limit: query.pageSize,
                    offset: (query.page * query.pageSize),
                    data
                },
            }
        ).then((response) => {

            return Promise.resolve({
                data: response.data.results,
                page: query.page,
                totalCount: parseInt(response.data.count)
            })
        })
            .catch((error) => {
                return Promise.reject(error);
            })

    }

    return (
        <div style={{height: '100%'}}>


            <AutoSizer disableWidth={true}>
                {({height, width}) => {
                    const pageSize = Math.floor((height - 700) / 67);

                    return (
                        <div style={{height: `${height}px`, width: `${width}px`, overflowY: 'auto'}}>

                            <Box
                                display="flex"
                                flexGrow={1}
                                alignItems="center"
                                flexDirection="column"
                            >
                                <Box
                                    display="flex"
                                    flexDirection="row"
                                    justifyContent="space-between"
                                    flexGrow={1}
                                    px={2}
                                >
                                    <Box
                                        display="flex"
                                        alignItems="center"
                                    >

                                        <Box
                                            mx={1}
                                        >
                                            <Typography variant="h6">
                                                <Box>Movies</Box>
                                            </Typography>
                                        </Box>

                                    </Box>

                                </Box>


                                <div style={{width: '100%'}}>
                                    <EGADataTableContext.Provider value={{
                                        getData,
                                        columns,
                                        selectedColumns,
                                        filters,
                                        pageSize,
                                    }}>
                                        <Egadatatable/>
                                    </EGADataTableContext.Provider>
                                </div>

                            </Box>

                        </div>
                    );
                }}
            </AutoSizer>

        </div>
    )
}
