import DateFnsUtils from "@date-io/date-fns";

export default function dateStringToLocalDate(s) {
    if (!s) return null;
    return new DateFnsUtils().parse(s, 'yyyy-MM-dd');
}
