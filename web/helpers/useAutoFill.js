import {useState} from 'react';
import useLocalStorage from "./useLocalStorage";

const useAutoFill = () => {
    const [autoFillData, setAutoFillData] = useLocalStorage('autofill', {});

    return [autoFillData, setAutoFillData];
}

export default useAutoFill;