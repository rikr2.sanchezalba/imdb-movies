module.exports = {
    basePath: '/web',
    webpack(config, {webpack, buildId}) {
        config.plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    BUILD_ID: JSON.stringify(buildId),
                },
            }),
        );

        config.module.rules.push(
            {
                test: /\.svg$/,
                use: ['@svgr/webpack']
            }
        );


        return config;
    },
}
