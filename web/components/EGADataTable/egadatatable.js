import {useContext, useState, useEffect} from 'react';
import EGADataTableContext from "./EGADataTableContext";
import AxiosContext from "../Auth/AxiosContext";
import MaterialTable, {MTableFilterRow, MTablePagination} from '@material-table/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import {Grid, TablePagination, Typography} from '@material-ui/core';
import {makeStyles, createTheme} from '@material-ui/core/styles';
import theme from "../../styles/theme";
import {fade} from '@material-ui/core/styles/colorManipulator';
import Box from "@material-ui/core/Box";


const useStyles = makeStyles((theme) => ({

}));


const Egadatatable = () => {


    const classes = useStyles();

    const {
        getData,
        columns,
        selectedColumns,
        filters,
        pageSize,
    } = useContext(EGADataTableContext)




    const getColumnsInOrder = () => {
        let cr = []
        selectedColumns.forEach((s) => {
            cr.push(columns.filter(c => {
                return (c.field === s)
            })[0])
        })

        return cr.map(col => {
            const df = filters.filter(item => (item.field === col.field))[0]
            return {...col, defaultFilter: (df ? df.value : '')};
        });
    }



    return (
        <>
            {selectedColumns && (selectedColumns.length > 0) &&
            <div style={{background: 'blue', height: `calc(100vh - 40px)`}}>


                    {pageSize > 0 &&
                    <MaterialTable
                        title="Movies"
                        data={getData}
                        actions={[]}
                        columns={columns}

                        options={{
                            filtering: true,
                            search: false,
                            sort: true,
                            debounceInterval: 1500,
                            pageSize: pageSize,
                            pageSizeOptions: [],
                            paginationType: 'normal',

                            cellStyle: {
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                                overflow: 'hidden',
                                maxWidth: 100
                            }
                        }}
                        style={{borderRadius: 0, height: '100%', padding: 0}}
                        components={{
                            Toolbar: props => (
                                <div>
                                </div>
                            ),
                            FilterRow: props => {
                                return (<MTableFilterRow {...props} hideFilterIcons={true}/>);
                            },
                            Pagination: props => {
                                return (
                                    <td style={{display: 'flex', width: '100%'}}>

                                        <Box
                                            style={{position: 'absolute', bottom: 0, width: '90%', paddingRight: '50px'}}

                                        >
                                            <Box
                                                style={{position: 'relative'}}
                                            >
                                                <Box
                                                    display='flex'
                                                    flexDirection='row'
                                                    justifyContent='flex-end'
                                                    alignItems='center'
                                                >
                                                    <Grid container justifyContent='flex-end'>
                                                        <Grid item>
                                                            <Box
                                                                display='flex'
                                                                alignItems='center'
                                                                flexDirection='row'
                                                                justifyContent='flex-start'
                                                                height='100%'
                                                                pr={2}
                                                            >
                                                                <Grid container spacing={2}>
                                                                    <Grid item>
                                                                        <Typography variant='caption'>
                                                                            Page: {props.page + 1}
                                                                        </Typography>
                                                                    </Grid>
                                                                    <Grid item>
                                                                        <Typography variant='caption'>
                                                                            Rows per page: {props.rowsPerPage}
                                                                        </Typography>
                                                                    </Grid>
                                                                    <Grid item>
                                                                        <Typography variant='caption'>
                                                                            In this page: {Math.min(
                                                                            (props.page + 1) * props.rowsPerPage,
                                                                            props.count
                                                                        ) - (props.count === 0
                                                                            ? 0
                                                                            : props.page * props.rowsPerPage + 1) + 1}
                                                                        </Typography>
                                                                    </Grid>
                                                                </Grid>


                                                            </Box>
                                                        </Grid>
                                                        <Grid item>
                                                            <Box
                                                                display='flex'
                                                                justifyContent='flex-end'
                                                            >
                                                                <Box
                                                                    width='350px'
                                                                >
                                                                    {(() => {
                                                                        const {
                                                                            classes: {
                                                                                selectRoot, caption, toolbar,
                                                                                ...pClasses
                                                                            }, ...pProps
                                                                        } = props
                                                                        return (

                                                                            <MTablePagination {...pProps} classes={{...pClasses}}/>

                                                                        );
                                                                    })()
                                                                    }

                                                                </Box>
                                                            </Box>

                                                        </Grid>
                                                    </Grid>
                                                </Box>

                                            </Box>

                                        </Box>
                                    </td>

                                );
                            }

                        }}
                    />
                    }



            </div>
            }
        </>
    );
}

export default Egadatatable;
