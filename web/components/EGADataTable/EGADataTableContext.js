import { createContext } from 'react';

const EGADataTableContext = createContext();

export default EGADataTableContext;
