import {useState, useMemo, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import useLocalStorage from "../../helpers/useLocalStorage";


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        gap: '5px',
        '& input': {
            maxWidth: '60px',
            borderRadius: '4px',
            border: '1px solid black',
            padding: '3px'
        }
    }
}));

const NumFilter = (props) => {

    const [min, setMin] = useState(props.filters[props.fieldName] ? parseInt(props.filters[props.fieldName].split(' ')[0]): 0)
    const [max, setMax] = useState(props.filters[props.fieldName] ? parseInt(props.filters[props.fieldName].split(' ')[1]): 9999)
    const classes = useStyles();

    useEffect(()=>{
        if(localStorage.getItem('movieFilters')){
            if(JSON.parse(localStorage.getItem('movieFilters')).filter(e => e.field === props.fieldName).length === 1){
                let v = JSON.parse(localStorage.getItem('movieFilters')).filter(e => e.field === props.fieldName)[0].value.split(' ')
                setMin(v[0])
                setMax(v[1])
            }
        }
    },[])

    return (
        <div className={classes.root}>
            <label for='min'>Min:</label>
            <input
                type='number'
                value={min}
                name='min'
                onChange={(event => {
                    props.onFilterChanged(props.columnDef.tableData.id, event.target.value + ' ' + max);
                    setMin(event.target.value)
                })}/>
            <label htmlFor='max'>Max:</label>
            <input
                type='number'
                value={max}
                name='max'
                onChange={(event => {
                    props.onFilterChanged(props.columnDef.tableData.id, min + ' ' + event.target.value);
                    setMax(event.target.value)
                })}/>
        </div>
    );
}

export default NumFilter;
