import {useEffect, useState, useContext} from 'react';
import AxiosContext from "../Auth/AxiosContext";
import Image from 'next/image';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    placeholder: {
        width:'45px',
        height:'67px',
        border: '1px solid black',
        background: 'linear-gradient(to top left, rgba(0,0,0,0) 0%,  rgba(0,0,0,0) calc(50% - 0.8px), rgba(0,0,0,1) 50%, rgba(0,0,0,0) calc(50% + 0.8px), rgba(0,0,0,0) 100%), linear-gradient(to top right, rgba(0,0,0,0) 0%,  rgba(0,0,0,0) calc(50% - 0.8px), rgba(0,0,0,1) 50%,rgba(0,0,0,0) calc(50% + 0.8px),rgba(0,0,0,0) 100%)'
    }
}));
const Egapostercell = (props) => {

    const classes = useStyles();
    const {axios} = useContext(AxiosContext);
    const [posterUrl, setPosterUrl] = useState('');

    function isValidHttpUrl(string) {
        let url;

        try {
            url = new URL(string);
        } catch (_) {
            return false;
        }

        return url.protocol === "http:" || url.protocol === "https:";
    }

    useEffect(() => {

        axios(
            {
                method: 'get',
                url: props.omdbUrl,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },

            }
        ).then((response) => {

            setPosterUrl(response.data.Poster)

        })
            .catch((error) => {
                console.log(error)
            })
    }, [props.omdbUrl]);

    return (
        <>

            {posterUrl && isValidHttpUrl(posterUrl) &&
                    <img src={posterUrl} alt={posterUrl} width='45px' height='67px'/>

            }
            {!isValidHttpUrl(posterUrl) &&
                <div className={classes.placeholder}>

                </div>
            }

        </>
    );

};

export default Egapostercell;