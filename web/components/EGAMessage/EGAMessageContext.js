import { createContext } from 'react';

const EGAMessageContext = createContext();

export default EGAMessageContext;