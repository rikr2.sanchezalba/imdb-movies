import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { useState, useContext } from 'react';
import EGAMessageContext from "./EGAMessageContext";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const EGAMessage = () => {

    const {open, severity, message, showMessage, autoHideDuration, handleClose} = useContext(EGAMessageContext);

    return (
        <>
            <Snackbar open={open} autoHideDuration={autoHideDuration} onClose={handleClose}>
                <Alert severity={severity} onClose={handleClose}>
                    {message}
                </Alert>
            </Snackbar>
        </>
    );
}

export default EGAMessage;