import {useState, useEffect} from 'react';

const useEGAMessage = () => {
    const [open, setOpen] = useState(false);
    const [severity, setSeverity] = useState('success');
    const [message, setMessage] = useState('');
    const [autoHideDuration, setAutoHideDuration] = useState(6000);
    const [handleClose, setHandleClose] = useState(()=>{
        return () =>{
            setOpen(false);
        }
    });

    const showMessage = (msg = 'Default message', severity = 'success', autoHideDuration = 6000) => {
        setMessage(msg)
        setSeverity(severity)
        setAutoHideDuration(autoHideDuration)
        setOpen(true);
    }

    return {open, severity, message, showMessage, autoHideDuration, handleClose}
}

export default useEGAMessage;