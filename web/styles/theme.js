import {createTheme} from '@material-ui/core/styles';
import {red} from '@material-ui/core/colors';

// Create a theme instance.
const theme = createTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#999199',
            text: '#FFFFFF'
        },
        secondary: {
            main: '#999191',
            text: '#FFFFFF'
        },
        error: {
            main: red.A400,
        },
        background: {
            default: '#fafafa',
            paper: '#fafafa',
            gray: '#ECECEC',
            lightgray: '#EEEEEE',
            carbon: '#5C6161',
        },
        text: {
            primary: '#000000',
            secondary: '#999199',
            white: '#FEFEFE'
        },
        textlink: {
            primary: '#3399DD',
        },
        appBar: {
            background: '#2f384f'
        },
    },
    typography: {
        fontFamily: 'SegoeUI',
    },
});

export default theme;
