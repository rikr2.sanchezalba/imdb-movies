#!/bin/bash

docker build -t egaetl --no-cache ./etl
docker run -e PYTHONUNBUFFERED=0 --network="host" egaetl
