# Generated by Django 2.2.14 on 2021-09-13 23:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('genre', models.CharField(max_length=300)),
                ('year', models.IntegerField()),
                ('rating', models.FloatField()),
                ('runtime', models.FloatField()),
                ('poster', models.CharField(max_length=2048)),
                ('link', models.CharField(max_length=2048)),
            ],
            options={
                'verbose_name': 'movie',
                'verbose_name_plural': 'movies',
            },
        ),
    ]
