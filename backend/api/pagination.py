from rest_framework import pagination
from rest_framework.response import Response
from os import environ

class EGAPagination(pagination.LimitOffsetPagination):

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link().replace('backend:8000', environ.get('HOST', default='localhost')) if self.get_next_link() is not None else 'null',
                'previous': self.get_previous_link().replace('backend:8000', environ.get('HOST', default='localhost')) if self.get_previous_link() is not None else 'null'
            },
            'count': self.count,
            'results': data
        })