from .serializers import *
from .models import *
from rest_framework import status, viewsets, views
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView

from rest_framework.viewsets import ViewSet, ModelViewSet
from .permission import *
from django.db.models import Q, F, Func, Value, Count, OuterRef, Subquery
import json
from .util import get_fieldset_filter_expr_for_model
from django.db.models.functions import Concat, Coalesce
from django.db.models import CharField, IntegerField, Max
from rest_framework.settings import api_settings
from rest_framework_csv.renderers import CSVRenderer
import ast
from .core.core import EGABaseViewSet
from django.contrib.postgres.aggregates import StringAgg, ArrayAgg


class MovieViewSet(EGABaseViewSet):
    serializer_class = MovieSerializer

    def get_queryset(self):
        queryset = Movie.objects.all()
        queryset = self.add_filter(Movie, queryset)

        return queryset.all()
