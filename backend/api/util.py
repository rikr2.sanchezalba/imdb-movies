from .models import *
from django.db.models import Q
from functools import reduce
import re

model_fields_map = {
    Movie: {
        'id': ('id', 'number'),
        'title': ('title', 'text'),
        'genres': ('genres', 'option'),
        'year': ('year', 'number'),
        'rating': ('rating', 'number'),
        'runtime': ('runtime', 'number'),
        'poster': ('poster', 'text'),
        'link': ('link', 'text')
    },

}


def get_op(tup):
    if tup[1] == 'number':
        return '__range'
    else:
        return '__icontains'


def get_value(tup, value):
    print(tup)
    print(value)
    if tup[1] == 'number':
        m = value.split(' ')
        print(m[0], m[1])
        return m[0], m[1]
    else:
        return value

def get_fieldset_filter_expr_for_model(cls, fields, values, fn_all_mtm, fv_all_mtm):
    m = model_fields_map.get(cls)  # field map
    l = [Q(**{m[field][0] + get_op(m[field]): get_value(m[field], value)}) for (field, value) in zip(fields, values)]
    l_mtm = [Q(**{m[field][0] + get_op(m[field]): get_value(m[field], value)}) for (field, value) in zip(fn_all_mtm, fv_all_mtm)]

    return reduce(lambda q1, q2: q1 & q2, l + l_mtm)
