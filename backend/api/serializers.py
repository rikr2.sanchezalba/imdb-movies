from rest_framework import serializers
from .models import *
from datetime import date
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
import json


class MovieSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(MovieSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = Movie
        fields = '__all__'