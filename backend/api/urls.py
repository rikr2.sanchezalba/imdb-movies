from django.urls import include, path

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('movies', views.MovieViewSet, basename='movie-list')

urlpatterns = [
    path('', include(router.urls))
]