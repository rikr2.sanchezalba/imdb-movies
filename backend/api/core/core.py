from rest_framework.viewsets import ModelViewSet
from rest_framework.settings import api_settings
from rest_framework_csv.renderers import CSVRenderer
from django.db.models import Q, F, Func, Value
from django.db.models.functions import Concat
from django.contrib.postgres.aggregates import ArrayAgg, StringAgg

import json
from ..util import get_fieldset_filter_expr_for_model

class EGAViewWithCsvResponse:

    def get_renderer(self):
        return tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (CSVRenderer,)

class EGAViewWithFilter:

    def add_filter(self, model_cls, queryset, mtm=[]):

        data = self.request.query_params.get('data', None)
        q = self.request.query_params.get('q', None)

        if q is not None:
            q_tokens = q.split()
            if len(q_tokens) == 1 and q_tokens[0].isdigit():
                queryset = queryset.filter(pk=q_tokens[0])
            else:
                f = Q()
                for q_t in q_tokens:
                    f &= Q(repr__icontains=q_t)
                queryset = queryset.filter(f)

        if data is not None:
            data = json.loads(data)
            print(data)
            if 'filters' in data.keys():
                filters = data['filters']

                fn_all = [f_item['column']['field'] for f_item in filters if f_item['column']['field'] not in mtm]
                fn_all_mtm = [f_item['column']['field'] for f_item in filters if f_item['column']['field'] in mtm]
                fv_all = [f_item['value'] for f_item in filters if f_item['column']['field'] not in mtm]
                fv_all_mtm = [f_item['value'] for f_item in filters if f_item['column']['field'] in mtm]

                if len(fn_all) > 0 or len(fn_all_mtm) > 0:
                    f = get_fieldset_filter_expr_for_model(model_cls, fn_all, fv_all, fn_all_mtm, fv_all_mtm)
                    queryset = queryset.filter(f)

            if 'orderBy' in data.keys():
                order = '-' if data['orderDirection'] == 'desc' else ''
                queryset = queryset.order_by(order + data['orderBy'])
        return queryset

class EGABaseViewSet(ModelViewSet, EGAViewWithCsvResponse, EGAViewWithFilter):

    def __init__(self, *args, **kwargs):
        super(EGABaseViewSet, self).__init__(*args, **kwargs)
        self.renderer_classes = self.get_renderer()

    def finalize_response(self, request, response, *args, **kwargs):
        response = super(EGABaseViewSet, self).finalize_response(request, response, *args, **kwargs)
        if request.META['HTTP_ACCEPT'] == 'text/csv':
            response['Content-Disposition'] = 'attachment;filename=export.csv'
            response.data = response.data['results']
        return response

