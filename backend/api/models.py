from django.db import models


class Movie(models.Model):
    tconst = models.CharField(max_length=50)
    title = models.CharField(max_length=300)
    genres = models.CharField(max_length=300)
    year = models.IntegerField()
    rating = models.FloatField()
    runtime = models.FloatField()
    poster = models.CharField(max_length=2048)
    link = models.CharField(max_length=2048)

    class Meta:
        verbose_name = 'movie'
        verbose_name_plural = 'movies'

    def __str__(self):
        return self.title

