# IMDB-Movies

## Installation:
```
cp web/.env.local.example web/.env.local
cp backend/.env.local.example backend/.env.local
docker-compose build --no-cache
docker-compose up -d
docker-compose exec backend python manage.py migrate
./run_etl.sh
docker-compose restart nginx
echo 'Open in browser: http://localhost/web'
 ```
