from sqlalchemy import create_engine
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from functools import reduce
from tqdm import tqdm


def convert_float(val):
    try:
        return pd.to_numeric(val)
    except:
        return 0.0

def convert_int(val):
    try:
        return pd.to_numeric(val)
    except:
        return 0



chunk_size=10000


print('Start Read')

l1  = sum(1 for line in open('data/title.basics.tsv', 'r'))
l2  = sum(1 for line in open('data/title.ratings.tsv', 'r'))


basics = pd.DataFrame()
basics_cnks = pd.read_csv('data/title.basics.tsv', 
                            sep='\t', 
                            usecols=['tconst','startYear', 'originalTitle', 'runtimeMinutes', 'genres', 'titleType'], 
                            converters={'runtimeMinutes': convert_float, 'startYear' : convert_int},
                            chunksize=chunk_size,
                            dtype={'tconst': str, 
                                'originalTitle': str,
                                'genres' : str, 
                                'titleType': str },
                                na_values=['\\N'])


print('End Read')



for chunk in tqdm(basics_cnks, total=l1//chunk_size):
    chunk['runtimeMinutes'] = chunk['runtimeMinutes'].fillna(0).astype(np.short)
    chunk['genres'] = chunk['genres'].fillna('')                    
    basics = pd.concat([basics, chunk[chunk['titleType']=='movie'].drop(['titleType'], axis=1)], ignore_index=True)



#basics = pd.read_csv('data/title.basics.tsv', sep='\t')
df0 = basics
df0

ratings = pd.DataFrame()
ratings_cnks = pd.read_csv('data/title.ratings.tsv', 
                        usecols=['tconst', 'averageRating'],
                        dtype={'tconst': str,
                                'averageRating': np.single},
                        chunksize=chunk_size,
                        na_values=['\\N'],
                                sep='\t')

for chunk in tqdm(ratings_cnks, total=l2//chunk_size):                    
    ratings = pd.concat([ratings, chunk], ignore_index=True)    



df1 = ratings
df1


dfs = [df0, df1]
df_final = reduce(lambda left,right: pd.merge(left,right,on='tconst'), dfs)


df_final = df_final.replace('\\N', 0)


# df_final = df_final.astype({"tconst": str, "startYear": int, 'originalTitle': str, 'runtimeMinutes': float, 'genres': str, 'averageRating': float})


df_final = df_final.rename(columns={'startYear' : 'year', 'originalTitle' : 'title', 'runtimeMinutes' : 'runtime', 'averageRating': 'rating'})


api_key='eaf34532'

def get_omdb_info_link (row):
    return "https://www.omdbapi.com/?i="+row['tconst']+"&apikey="+api_key


def get_imbd_link (row):
    return 'https://www.imdb.com/title/'+row['tconst']+'/'


df_final['poster'] = df_final.apply(lambda row : get_omdb_info_link(row), axis=1)
df_final['link'] = df_final.apply(lambda row : get_imbd_link(row), axis=1)


engine = create_engine('postgresql://egamovies:egamovies123@localhost:5432/egadb')


df_final.to_sql('api_movie', engine, if_exists='append', index=False)



