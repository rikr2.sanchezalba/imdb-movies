#!/bin/bash

mkdir -p data
while read link; do
  echo "downloading ${link##*/}"
  wget  "$link" -O "data/${link##*/}"
done < files.txt

ls data/
echo "extracting data..."

for filename in data/*.gz; do
  gzip -d "$filename"
done

python3 -u explore.py
